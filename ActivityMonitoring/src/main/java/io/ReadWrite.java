package io;


import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import model.*;

public class ReadWrite {
	public static void saveLog(Map<String, Integer> t, String filename){
		try{
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename));
			out.writeObject(t);
			out.close();
			} catch(Exception e){
				System.out.println("err"+e.toString());
			}
	}
	
	public static void saveLog2(Map<Integer, Map<String, Integer>> t, String filename){
		try{
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename));
			out.writeObject(t);
			out.close();
			} catch(Exception e){
				System.out.println("err"+e.toString());
			}
	}
	
	public static void saveLog3(Map<String, DateTime> t, String filename){
		try{
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename));
			out.writeObject(t);
			out.close();
			} catch(Exception e){
				System.out.println("err"+e.toString());
			}
	}
	
	public static void saveLog4(List<String>t, String filename){
		try{
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename));
			out.writeObject(t);
			out.close();
			} catch(Exception e){
				System.out.println("err"+e.toString());
			}
	}
	
	interface MyString {
		ArrayList<String> myStringFunction(String fn);
	}
	
	@SuppressWarnings("deprecation")
	public static ArrayList<MonitoredData> readActivity(String filename){
		try{
			MyString ms = (fn) -> {
				java.util.List<String> lines = new ArrayList<String>();
				
				@SuppressWarnings("resource")
				DataInputStream dataIn;
				try {
					dataIn = new DataInputStream(new FileInputStream(fn));
					while(dataIn.available() > 0) {
				         lines.add(dataIn.readLine());
				    }
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return (ArrayList<String>) lines;
			};
			
			
			//lines = Files.readAllLines(Paths.get(filename), StandardCharsets.UTF_8);
			ArrayList<MonitoredData> t = new ArrayList<MonitoredData>();
			java.util.List<String> lines = ms.myStringFunction(filename);
		    int s = lines.size();
		    for (int i = 0; i < s; i++) {
		    	SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
			    Date ds = ft.parse(lines.get(i).substring(0, 19));
			    Date de = ft.parse(lines.get(i).substring(21, 40));
			    String a = lines.get(i).substring(42, lines.get(i).length());
			    t.add(new MonitoredData(ds, de, a));
		    }
			return t;
		} catch(Exception e){
			System.out.println("error: "+e.toString());
			return null;
		}
	}

	/*public static boolean restoreBank(String filename) {
		Map<Person,Set<Account>> tr = new HashMap<Person,Set<Account>>();
		if (readBank(filename) != null) {
			tr = readBank(filename);
			return true;
		} else return false;
	}*/
}

