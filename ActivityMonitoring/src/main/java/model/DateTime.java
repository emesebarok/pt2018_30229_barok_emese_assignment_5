package model;

import java.io.Serializable;

public class DateTime implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long hours;
	private long minutes;
	private long seconds;
	
	public DateTime (long h, long m, long s) {
		this.hours = h;
		this.minutes = m;
		this.seconds = s;
	}

	public long getHours() {
		return hours;
	}

	public void setHours(long hours) {
		this.hours = hours;
	}

	public long getMinutes() {
		return minutes;
	}

	public void setMinutes(long minutes) {
		this.minutes = minutes;
	}

	public long getSeconds() {
		return seconds;
	}

	public void setSeconds(long seconds) {
		this.seconds = seconds;
	}
	
	
}
