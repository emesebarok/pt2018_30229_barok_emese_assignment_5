package PT2018.ActivityMonitoring;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.ReadWrite;
import model.DateTime;
import model.MonitoredData;

public class App 
{
	
    public static void main( String[] args )
    {
        ArrayList<MonitoredData> m = new ArrayList<MonitoredData>();
        
        //reading from file
        m = ReadWrite.readActivity("activity.txt");
        //int s = m.size();
        //SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
        //for(int i = 0; i < s; i++) System.out.println(ft.format(m.get(i).getStartTime()) + " " + ft.format(m.get(i).getEndTime()) + " " + m.get(i).getActivity());
        
        //Counting the distinct days that appear in the monitoring data
        @SuppressWarnings("deprecation")
		MyCounting c = (counter) -> {
        	int db = 1;
        	int l = counter.size();
        	for (int i = 0; i < l; i++) {
        		if (counter.get(i).getEndTime().getDate() - counter.get(i).getStartTime().getDate() != 0) db++;
        		if (i != 0 && counter.get(i - 1).getEndTime().getDate() - counter.get(i).getStartTime().getDate() != 0) db++; 
        	}
        	return db;
        };
        
        //System.out.println(c.myCounting(m));
        
        //Determine a map of type <String, Integer> that maps to each distinct action type the number of occurrences in the log.
        MyActivity ma = (md) -> {
        	Map<String, Integer> a = new HashMap<String, Integer>();
        	String []activities = { "Leaving", "Toileting", "Showering", "Sleeping", "Breakfast", "Lunch", "Dinner", "Snack", "Spare_Time/TV", "Grooming"};
        	int l = md.size();
        	int ac = activities.length;
        	int counter[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        	int j;
        	for (int i = 0; i < l; i++) {
        		j = 0;
        		while (!md.get(i).getActivity().contains(activities[j]) && j < ac) {
        			j++;
        		}
        		if (j != ac) counter[j]++;
        	}
        	for(int i = 0; i < ac; i++) {
            	a.put(activities[i], counter[i]);
            }
        	return a;
        };
        
        ReadWrite.saveLog(ma.myActivity(m), "log.txt");
        
        /*
        * Generates a data structure of type Map<Integer, Map<String, Integer>> that
        * contains the activity count for each day of the log (task number 2 applied for each
        * day of the log)and writes the result in a text file
        */
        
        @SuppressWarnings("deprecation")
		MyActivityCounting mac = (md) -> {
        	ArrayList<MonitoredData> helper = new ArrayList<MonitoredData>();
        	Map<Integer, Map<String, Integer>> returner = new HashMap<Integer, Map<String, Integer>>();
        	int mdSize = md.size();
        	int dc = 0;
        	for(int i = 0; i < mdSize; i++) {
        		if (md.get(i).getEndTime().getDate() - md.get(i).getStartTime().getDate() == 0) {
        			helper.add(md.get(i));
        		}
        		else {
        			helper.add(md.get(i));
        			Map<String, Integer> helper2 = ma.myActivity(helper);
        			returner.put(dc, helper2);
        			dc++;
        			helper.clear();
        			helper.add(md.get(i));
        		}
        	}
        	Map<String, Integer> helper2 = ma.myActivity(helper);
			returner.put(dc, helper2);
        	return returner;
        };
        
        /*String []activities = { "Leaving", "Toileting", "Showering", "Sleeping", "Breakfast", "Lunch", "Dinner", "Snack", "Spare_Time/TV", "Grooming"};
        Map<Integer, Map<String, Integer>> name = mac.myActivityCounting(m);
        int dc = c.myCounting(m);
        for(int i = 0; i < dc; i++) {
        	System.out.println(i + ": ");
        	for (int j = 0; j < 10; j++) {
        		System.out.println(activities[j] + ": " + name.get(i).getOrDefault(activities[j], 0));
        	}
        }*/
        
        ReadWrite.saveLog2(mac.myActivityCounting(m), "log2.txt");
        
        /*
         * 
         * Determine a data structure of the form Map<String, DateTime> that maps
         * for each activity the total duration computed over the monitoring period. Filter the
         * activities with total duration larger than 10 hours. Write the result in a text file.
         * 
         */
        
        MyDuration d = (md) -> {
        	Map<String, DateTime> a = new HashMap<String, DateTime>();
        	String []activities = { "Leaving", "Toileting", "Showering", "Sleeping", "Breakfast", "Lunch", "Dinner", "Snack", "Spare_Time/TV", "Grooming"};
        	int l = md.size();
        	int ac = activities.length;
        	long[] counter = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        	int j;
        	for (int i = 0; i < l; i++) {
        		j = 0;
        		while (!md.get(i).getActivity().contains(activities[j]) && j < ac) {
        			j++;
        		}
        		if (j != ac) {
        			counter[j] += (md.get(i).getEndTime().getTime() - md.get(i).getStartTime().getTime());
        		}
        	}
        	for(int i = 0; i < ac; i++) {
        		long h = TimeUnit.MILLISECONDS.toHours(counter[i]);
        		long min = (TimeUnit.MILLISECONDS.toMinutes(counter[i]) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(counter[i])));
        		long sec = (TimeUnit.MILLISECONDS.toSeconds(counter[i]) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(counter[i])));
        		DateTime helper = new DateTime(h, min, sec);
        		//System.out.println(activities[i] + ": " + helper.getHours() + ":" + helper.getMinutes() + ":" + helper.getSeconds() + "\t" + counter[i]);
            	a.put(activities[i], helper);
            }
        	return a;
        };
        
        //filtering
		MyFilter mf = (p) -> {
        	Map<String, DateTime> a = new HashMap<String, DateTime>();        	
        	String []activities = { "Leaving", "Toileting", "Showering", "Sleeping", "Breakfast", "Lunch", "Dinner", "Snack", "Spare_Time/TV", "Grooming"};
        	int n = activities.length;
        	for(int i = 0; i < n; i++) {
        		if(p.get(activities[i]).getHours() > 10) {
        			a.put(activities[i], p.get(activities[i]));
        			//System.out.println(activities[i]);
        		}
        	}
        	return a;
        };
        //System.out.println("Total duration bigger then 10 hours: ");
        ReadWrite.saveLog3(mf.myFilter(d.myDuration(m)), "log3.txt");
        
        /*
         * Filter the activities that have 90% of the monitoring samples with duration
         * less than 5 minutes, collect the results in a List<String> containing only the
         * distinct activity names and write the result in a text file.
         */
        
        MyLastFilter mlf = (f, md) -> {
        	List<String> list = new ArrayList<String>();
        	String []activities = { "Leaving", "Toileting", "Showering", "Sleeping", "Breakfast", "Lunch", "Dinner", "Snack", "Spare_Time/TV", "Grooming"};
        	int l = md.size();
        	int ac = activities.length;
        	int counter[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        	int j;
        	for (int i = 0; i < l; i++) {
        		j = 0;
        		while (!md.get(i).getActivity().contains(activities[j]) && j < ac) {
        			j++;
        		}
        		if (j != ac) {
        			long helper = md.get(i).getEndTime().getTime() - md.get(i).getStartTime().getTime();
        			long h = TimeUnit.MILLISECONDS.toHours(helper);
            		long min = (TimeUnit.MILLISECONDS.toMinutes(helper) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(helper)));
        			if (h == 0 && min < 5) counter[j]++;
        		}
        	}
        	for (int i = 0; i < ac; i++) {
        		if (counter[i] == f.get(activities[i])) {
        			list.add(activities[i]);
        			//System.out.println(activities[i]);
        		}
        	}
        	return list;
        };
        //System.out.println("Filter#2: ");
        ReadWrite.saveLog4(mlf.myLastFilter(ma.myActivity(m), m), "log4.txt");
    }
    
    //task#5
    interface MyLastFilter {
    	List<String> myLastFilter(Map<String, Integer> f, ArrayList<MonitoredData> md);
    }
    
    //task#4 for filter
    interface MyFilter {
    	Map<String, DateTime> myFilter(Map<String, DateTime> p);
    }
    
    //task#4
    interface MyDuration {
    	Map<String, DateTime> myDuration(ArrayList<MonitoredData> md);
    }
    
    //task#3
    interface MyActivityCounting{
    	Map<Integer, Map<String, Integer>> myActivityCounting(ArrayList<MonitoredData> md);
    }
    
    //task#2
    interface MyActivity{
    	Map<String, Integer> myActivity(ArrayList<MonitoredData> md);
    }
    
    //task#1
    interface MyCounting {
		int myCounting(ArrayList<MonitoredData> counter);
	}
    
    
}
